# Tutorial: Configurando Filas, Tópicos e Bindings no RabbitMQ via rabbitmqctl

Este tutorial irá guiá-lo na configuração do RabbitMQ para o seu sistema de captura de vídeo utilizando o `rabbitmqctl`, a ferramenta de linha de comando do RabbitMQ.

**Pré-requisitos:**

* Instância do RabbitMQ em execução (local ou na nuvem).
* Acesso ao terminal ou prompt de comando com o `rabbitmqctl` instalado e configurado.

## Passo 1: Criar as Filas

Abra o terminal e execute os seguintes comandos para criar as filas:

```bash
rabbitmqctl declare_queue name=queue.captura.input durable=true
rabbitmqctl declare_queue name=queue.captura.output durable=true
rabbitmqctl declare_queue name=queue.captura.event durable=true
```

**Explicação:**

* `rabbitmqctl declare_queue`: Comando para declarar uma nova fila.
* `name`: Nome da fila (queue.captura.input, queue.captura.output, queue.captura.event).
* `durable=true`: Torna a fila persistente, para que as mensagens não sejam perdidas em caso de reinicialização do servidor (opcional, mas recomendado).

## Passo 2: Criar a Exchange "topic.captura.events

```bash
rabbitmqctl declare_exchange name=topic.captura.events type=topic durable=true
```

**Explicação:**

* `rabbitmqctl declare_exchange`: Comando para declarar uma nova exchange.
* `name`: Nome da exchange (topic.captura.events).
* `type`: Tipo da exchange (topic).
* `durable=true`: Torna a exchange persistente (opcional, mas recomendado).

## Passo 3: Criar os Bindings (Ligações)

```bash
rabbitmqctl bind_queue queue=queue.captura.output exchange=topic.captura.events routing_key="captura.output"
rabbitmqctl bind_queue queue=queue.captura.event exchange=topic.captura.events routing_key="#"
```

**Explicação:**

* `rabbitmqctl bind_queue`: Comando para ligar uma fila a uma exchange.
* `queue`: Nome da fila a ser ligada.
* `exchange`: Nome da exchange à qual a fila será ligada.
* `routing_key`: Chave de roteamento que define o padrão de mensagens que a fila receberá da exchange.
  * `captura.output`: A fila `queue.captura.output` receberá apenas mensagens com esta routing key.
  * `#`: A fila `queue.captura.event` receberá todas as mensagens da exchange, independentemente da routing key.

**Verificando a Configuração:**

Você pode verificar se as filas, a exchange e os bindings foram criados corretamente usando o comando:

```bash
rabbitmqctl list_queues
rabbitmqctl list_exchanges
rabbitmqctl list_bindings
```

**Observações:**

* **Usuário e Senha:** Se você estiver usando um servidor RabbitMQ com autenticação, pode ser necessário fornecer as credenciais de usuário e senha ao executar os comandos `rabbitmqctl`.
* **Ambiente Virtual:** Se você estiver usando um ambiente virtual Python, ative-o antes de executar os comandos `rabbitmqctl`.

## Diagrama de Arquitetura

```plantuml
@startuml
left to right direction

actor Usuário as user
actor Administrador as admin

cloud AWS {
  component "API Gateway" as gateway
  component "Evento Captura Input (Lambda)" as eventCapturaInput
  component "Evento Captura Output (Lambda)" as eventCapturaOutput
}

node RabbitMQ {
  queue "topic.captura.events" as topicExchange
  note right of topicExchange
    Tipo: Topic
  end note
  queue "queue.captura.output" as directExchange
  note right of directExchange
    Tipo: Direct
  end note
  queue "queue.captura.input" as inputQueue
}

node "Processamento" {
  component "Captura Video (Backend) (Java)" as capturaVideo
}

node "Administração" {
  component "Portal (Backend)" as portalBackend
  agent "Portal (Frontend)" as portalFrontend
}

gateway --> eventCapturaInput : Lambda/GET
eventCapturaInput --> inputQueue
capturaVideo .left.> inputQueue
capturaVideo --> topicExchange
directExchange <. topicExchange
portalBackend .left.> topicExchange
admin -down-> portalFrontend : SSE
portalFrontend --> portalBackend

eventCapturaOutput .up.> directExchange
@enduml
```

## Provas de Conceito em Python

As provas de conceito em Python para publicar e consumir mensagens nas filas e tópicos configurados neste tutorial podem ser encontradas no seguinte repositório:

[https://gitlab.com/catalogar-videos/provas-de-conceito/rabbitmq](https://gitlab.com/catalogar-videos/provas-de-conceito/rabbitmq)

Com este tutorial e os exemplos de código no repositório, você poderá configurar e testar a comunicação assíncrona entre os componentes do seu sistema de captura de vídeo utilizando o RabbitMQ.
