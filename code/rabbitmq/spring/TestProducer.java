@SpringBootTest
class RabbitMQProducerTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @MockBean
    private RabbitMQConsumer rabbitMQConsumer; // Mock do consumidor

    @Test
    void sendMessageTest() {
        String message = "Test message";
        rabbitTemplate.convertAndSend("my-exchange", "my-key", message);

        // Verifica se a mensagem foi enviada para a exchange correta
        ArgumentCaptor<String> exchangeCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> routingKeyCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);
        verify(rabbitTemplate).convertAndSend(exchangeCaptor.capture(), routingKeyCaptor.capture(), messageCaptor.capture());
        assertEquals("my-exchange", exchangeCaptor.getValue());
        assertEquals("my-key", routingKeyCaptor.getValue());
        assertEquals(message, messageCaptor.getValue());
    }
}
