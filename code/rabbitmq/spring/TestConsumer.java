@SpringBootTest
class RabbitMQConsumerTest {

    @Autowired
    private RabbitMQConsumer rabbitMQConsumer;

    @Test
    void receiveMessageTest() {
        String message = "Test message";
        rabbitMQConsumer.receiveMessage(message);

        // Verifica se a mensagem foi processada corretamente (implementação depende da sua lógica)
        // ...
    }
}