@Service
public class RabbitMQConsumer {

    @RabbitListener(queues = "${rabbitmq.queue.name}")
    public void receiveMessage(String message) {
        System.out.println("Mensagem recebida: " + message);
        // Processar a mensagem
    }
}