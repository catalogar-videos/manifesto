@ApplicationScoped
public class RabbitMQConsumer {

    @Incoming("amqp-in")
    public void receiveMessage(String message) {
        System.out.println("Mensagem recebida: " + message);
        // Processar a mensagem
    }
}
