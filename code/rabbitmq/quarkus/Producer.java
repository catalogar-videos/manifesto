@ApplicationScoped
public class RabbitMQProducer {

    @Inject
    @Channel("amqp-out")
    Emitter<String> emitter;

    public void sendMessage(String message) {
        emitter.send(message);
    }
}