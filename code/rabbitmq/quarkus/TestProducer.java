@QuarkusTest
class RabbitMQProducerTest {

    @InjectMock
    RabbitMQProducer producer; // Mock do produtor

    @Inject
    @Channel("amqp-out")
    Emitter<String> emitter;

    @Test
    void sendMessageTest() {
        String message = "Test message";
        producer.sendMessage(message);

        // Verifica se a mensagem foi enviada para o emitter
        verify(emitter).send(message);
    }
}