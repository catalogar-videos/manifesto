@QuarkusTest
class RabbitMQConsumerTest {

    @Inject
    RabbitMQConsumer consumer;

    @Test
    void receiveMessageTest() {
        String message = "Test message";
        consumer.receiveMessage(message);

        // Verifica se a mensagem foi processada corretamente (implementação depende da sua lógica)
        // ...
    }
}
