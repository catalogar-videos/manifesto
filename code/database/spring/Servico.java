@Service
public class PessoaService {

    @Autowired
    private PessoaRepository repository;

    public List<Pessoa> findAll() {
        return repository.findAll();
    }

    public Pessoa findById(String id) {
        return repository.findById(id).orElse(null);
    }

    public Pessoa save(Pessoa pessoa) {
        return repository.save(pessoa);
    }

    public void deleteById(String id) {
        repository.deleteById(id);
    }
}