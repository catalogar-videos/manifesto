@DataMongoTest // Anotação para testes com MongoDB
class PessoaRepositoryTest {

    @Autowired
    private PessoaRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate; // Para operações diretas no MongoDB

    @AfterEach
    void tearDown() {
        mongoTemplate.dropCollection(Pessoa.class); // Limpa a coleção após cada teste
    }

    @Test
    void testSaveAndFindById() {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome("João");
        pessoa.setIdade(30);
        repository.save(pessoa);

        Pessoa pessoaEncontrada = repository.findById(pessoa.getId()).orElse(null);
        assertNotNull(pessoaEncontrada);
        assertEquals("João", pessoaEncontrada.getNome());
        assertEquals(30, pessoaEncontrada.getIdade());
    }

    // ... outros testes
}
