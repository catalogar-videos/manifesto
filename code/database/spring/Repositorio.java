@Repository
public interface PessoaRepository extends MongoRepository<Pessoa, String> {
    // Métodos de consulta personalizados (opcional)
}