@ApplicationScoped
public class PessoaRepository {

    @Inject
    MongoClient mongoClient;

    public List<Pessoa> listAll() {
        return mongoClient.getDatabase("mydatabase")
                .getCollection("pessoas", Pessoa.class)
                .find()
                .list();
    }

    // ... outros métodos de consulta e manipulação
}
