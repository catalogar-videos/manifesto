@QuarkusTest
@QuarkusTestResource(MongoTestResource.class) // Inicia um MongoDB em memória para testes
class PessoaRepositoryTest {

    @Inject
    PessoaRepository repository;

    @InjectMongoClient("test") // Injeta o cliente MongoDB para o banco de dados de teste
    MongoClient mongoClient;

    @Test
    void testListAll() {
        Pessoa pessoa1 = new Pessoa();
        pessoa1.setNome("Maria");
        pessoa1.setIdade(25);

        Pessoa pessoa2 = new Pessoa();
        pessoa2.setNome("Pedro");
        pessoa2.setIdade(40);

        mongoClient.getDatabase("mydatabase")
                .getCollection("pessoas", Pessoa.class)
                .insertMany(List.of(pessoa1, pessoa2));

        List<Pessoa> pessoas = repository.listAll();
        assertEquals(2, pessoas.size());
        assertTrue(pessoas.stream().anyMatch(p -> p.getNome().equals("Maria")));
        assertTrue(pessoas.stream().anyMatch(p -> p.getNome().equals("Pedro")));
    }

    // ... outros testes
}