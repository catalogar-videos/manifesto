@BsonIgnoreExtraElements // Ignora campos extras no documento MongoDB
public class Pessoa {
    @BsonId
    private ObjectId id;
    private String nome;
    private int idade;
    // ... getters e setters
}
