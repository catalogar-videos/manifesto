@Service
public class PessoaService {

    @Autowired
    private PessoaRepository repository;

    public List<Pessoa> listAll() {
        return repository.listAll();
    }
    
}