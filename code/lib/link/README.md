# Biblioteca `captura-video-link`

## Descrição

A biblioteca `captura-video-link` fornece uma estrutura abstrata e flexível para a captura de links de vídeo de diversas fontes. Ela permite a integração com diferentes provedores de vídeo e oferece uma interface unificada para a obtenção e processamento dos links.

## Diagrama de Classes

```plantuml
@startuml

Interface Response  {
    int status()
    String videoFilePath()
}

interface Channel {
    void send(Message message)
}


interface VideoSourceEntry {
    Video video(Dictionary<String,String> parameters)
    String[] requiredParameters()
}

abstract class Link  {
    + Response get(PathFileDecorate path)
    ~ String fileExtension()
    ~ String path()
}

abstract class VideoSourceLinkCapture {
    - VideoLinkFetcher videoLinkFetcher
    - Channel channel
    ~ Link[] links(Dictionary<String,String> parameters)
    ~ Metadata metadata(Dictionary<String,String> parameters)
}

Link -- Response

VideoSourceLinkCapture .up.|> VideoSourceEntry: <<Implements>>

VideoSourceLinkCapture .up.> Channel : <<Dependency>>

VideoSourceLinkCapture *.right..> Link : contains

@endtuml
```

## Componentes Principais

* **`Response`:** Interface que define a estrutura de dados retornada após a busca por um link de vídeo, contendo o status da operação e o caminho do arquivo de vídeo.
* **Interface `PathFileDecorate`:** Introduzida para encapsular o nome de arquivo e oferecer maior flexibilidade na manipulação de caminhos. Implementa a interface `PathFile` para manter a compatibilidade com as funcionalidades existentes de acesso ao caminho completo e à extensão do arquivo.
* **`PathFile`:** Interface que define o caminho de um arquivo e sua extensão.
* **`VideoLinkFetcher` Refinado:** Agora, recebe um objeto `PathFileDecorate` em vez de strings separadas para o nome do arquivo e o caminho. Essa mudança torna a interface mais coesa e alinhada com o conceito de encapsulamento.
* **`Channel`:** Interface genérica para envio de mensagens assíncronas dentro do sistema, permitindo a comunicação entre componentes.
* **`VideoSourceEntry`:** Interface que representa uma entrada de fonte de vídeo, fornecendo acesso ao vídeo e seus parâmetros obrigatórios.
* **`VideoSourceLinkCapture` Simplificada:** Removeu o método `extension()`, já que a extensão do arquivo agora é acessível através da interface `PathFile`. Além disso, o método `paths()` retorna um array de objetos `PathFile`, proporcionando informações mais completas sobre os arquivos de vídeo.

## Como Usar

### Dependência Maven

Para incluir a biblioteca em seu projeto Maven, adicione a seguinte dependência ao arquivo `pom.xml`:

```xml
<dependency>
  <groupId>br.knin.project.lib</groupId>
  <artifactId>captura-video-link</artifactId>
  <version>1.5.9</version>
</dependency>
```

E o seguinte repositório:

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/58474776/packages/maven</url>
  </repository>
</repositories>
```

## Acesso ao Projeto

O código-fonte e a documentação completa da biblioteca estão disponíveis em:

[https://gitlab.com/catalogar-videos/bibliotecas/captura-video-link](https://gitlab.com/catalogar-videos/bibliotecas/captura-video-link)
