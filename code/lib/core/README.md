# Captura Video Core: Biblioteca Java para Captura de Vídeo

## Visão Geral

A Captura Video Core é uma biblioteca Java projetada para simplificar a captura de vídeos de diversas fontes. Ela
oferece uma estrutura modular e extensível para lidar com diferentes formatos e protocolos, sendo implementada em Java
puro para garantir portabilidade e compatibilidade entre plataformas. Você pode encontrar a biblioteca no seguinte
link: [https://gitlab.com/catalogar-videos/bibliotecas/captura-video-core](https://gitlab.com/catalogar-videos/bibliotecas/captura-video-core)

Para utilizar a biblioteca em seu projeto Maven, adicione as seguintes configurações ao seu arquivo `pom.xml`:

```xml

<dependency>
    <groupId>br.knin.project.lib</groupId>
    <artifactId>captura-video-core</artifactId>
    <version>1.4.1</version>
</dependency>

<repositories>

<repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/projects/58406128/packages/maven</url>
</repository>

</repositories>


```

A seção `<dependency>` informa ao Maven que seu projeto depende da biblioteca `captura-video-core`, versão 1.0. O Maven
baixará automaticamente a biblioteca do repositório especificado e a incluirá no classpath do seu projeto.

A seção `<repositories>` informa ao Maven onde encontrar a biblioteca. A URL especificada aponta para um repositório
Maven hospedado no GitLab, onde a biblioteca `captura-video-core` está disponível.

## Arquitetura

A biblioteca adota uma arquitetura orientada a interfaces, facilitando a integração com diversas fontes de vídeo e
mecanismos de armazenamento. Os principais componentes são:

- **`Channel`:** Interface para comunicação assíncrona entre os componentes da biblioteca, permitindo o envio de
  mensagens com eventos e metadados.
- **`Message`:** Representa uma mensagem enviada pelo canal, contendo um evento e metadados associados.
- **`Information
+`:** Estrutura chave-valor para armazenar informações adicionais sobre os eventos.
- **`Event`:** Enumeração que define os possíveis eventos relacionados à captura de vídeo (fonte encontrada, vídeo
  capturado, vídeo armazenado, etc.).
- **`VideoSource`:** Interface que representa uma fonte de vídeo. Cada fonte deve implementar essa interface para
  fornecer um método de captura de vídeo.
- **`VideoSourceEntry`:** Interface que representa uma entrada de fonte de vídeo, contendo informações sobre os
  parâmetros necessários para criar uma instância de `VideoSource`.
- **`Video`:** Interface que representa um vídeo capturado.
- **`Dictionary`:** Interface genérica para representar um dicionário de chave-valor.
- **`Repository`:** Interface para um repositório de fontes de vídeo, permitindo a busca por nome.
- **`Factory<T>`:** Interface genérica de fábrica para criar instâncias de objetos.
- **`Raw`:** Interface que representa dados brutos (bytes) do vídeo.
- **`Storage`:** Interface para serviços de armazenamento de vídeo.
- **`Resume`:** Interface que representa um resumo da operação de armazenamento de vídeo.

## Padrões de Design

A biblioteca utiliza os seguintes padrões de design para aumentar sua flexibilidade e extensibilidade:

- **Proxy:** Adiciona funcionalidades extras (logging, caching, controle de acesso) às fontes de vídeo e serviços de
  armazenamento sem modificar suas interfaces.
- **Null Object:** Fornece implementações "nulas" de fontes de vídeo e vídeos, evitando a necessidade de verificar se um
  objeto é nulo.
- **Factory:** Cria instâncias de fontes de vídeo de forma consistente e desacoplada.

## Implementação

A Captura Video Core é implementada em Java puro, sem dependências externas, garantindo portabilidade e compatibilidade.

## Uso

Para utilizar a biblioteca:

1. Implemente as interfaces `VideoSource`, `VideoSourceEntry`, `Storage` e `Channel` para suas fontes de vídeo, serviços
   de armazenamento e mecanismos de comunicação.
2. Crie uma instância do `Repository` e registre suas fontes de vídeo.
3. Use o `Repository` para obter uma instância de `VideoSource` e chame o método `video()` para capturar o vídeo.
4. Utilize o serviço de armazenamento (`Storage`) para armazenar o vídeo capturado.
5. Envie mensagens pelo canal (`Channel`) para notificar sobre os eventos de captura e armazenamento.

## Exemplo de Uso

```java
// ... (Configuração do repositório, canal e armazenamento)
Channel channel = ...
VideoSource fonteVideo = repositorio.source("minha_fonte_video", channel);
Video video = fonteVideo.video(parametros);
Resume resume = video.write(storage);
Event.VIDEO_AVAILABLE.send(channel);
```

## Diagrama de Classes

O diagrama de classes ilustra a estrutura da biblioteca, mostrando as interfaces, classes e seus relacionamentos. As
classes `VideoSourceProxy` e `StorageProxy` implementam o padrão Proxy para adicionar funcionalidades extras aos
componentes `VideoSource` e `Storage`. As classes `VideoSourceNullObject` e `VideoNullObject` implementam o padrão Null
Object para as interfaces `VideoSource` e `Video`, respectivamente. A interface `Factory` é responsável por criar
instâncias de `VideoSourceEntry`.

```plantuml
@startuml

interface Channel {
    void send(Message message)
}

interface Message {
    Event event()
    Information[] informations()
}

interface Information {
    String key()
    String value()
}

enum Event {

    VIDEO_SOURCE_FOUND
    VIDEO_CAPTURED
    VIDEO_STORED
    VIDEO_AVAILABLE
    PARAMETERS_NOT_PROVIDED
    VIDEO_SOURCE_NOT_FOUND
    VIDEO_NOT_FOUND
    VIDEO_SOURCE_UNAVAILABLE
    STORAGE_UNAVAILABLE
    VIDEO_NOT_STORED
}

interface VideoSource {
    Video video(Dictionary<String,String> parameters)
}

interface VideoSourceEntry {
    String[] requiredParameters()
}

interface Video {
    void write(Storage storage)
}

interface Dictionary<A,B>{
    B value(A)
    Bool hasKeys(A[])
}

interface Repository {
    VideoSource source(String videoSourceName, Channel channel)
}

interface Factory<T> {
    T build(Channel channel)
}

interface Raw {

    Metadata metadata()
    String pathLocalFile()
}

interface Metadata {
    String name()
    String autor()
    Date createDate()
    String[] tags()
}

interface Storage {
    void write(Raw raw)
}

interface Resume{
  String path()
  String name()
  Date date()
}

class VideoSourceProxy {
    - VideoSourceEntry instance
}

class FactoryNullObject{
    - VideoNullObject video
}

class VideoNullObject{

}

class StorageProxy {
    - Storage storage
}

VideoSourceEntry --|> VideoSource : <<Extends>>
VideoSource *-right-> Video
VideoSource .down.> Dictionary : <<Dependency>>
Repository *-down- Factory : <<Factory<VideoSourceEntry>>>
Repository .down.> Channel  : <<Dependency>>
Factory -left- VideoSourceEntry
Factory .right.> Channel : <<Dependency>>
FactoryNullObject .down.|> Factory :<<Implements>>
Channel ..> Message : <<Dependency>>
Message -right- Event
Message o-down- Information : <<Composite>>
Storage -right-> Resume
Storage .down.> Raw  : <<Dependency>>
StorageProxy .up.|> Storage: <<Implements>>

VideoSourceProxy ..|> VideoSource : <<Implements>>
VideoSourceProxy .right.> VideoSourceEntry
VideoNullObject .up.|> Video: <<Implements>>
Video .right.> Storage: <<Dependency>>

Raw -- Metadata
@enduml
```

## Diagrama de Atividade

O diagrama de atividade descreve o fluxo de trabalho da biblioteca, desde a recepção do evento de captura até a
notificação da disponibilidade do vídeo.

```plantuml
@startuml

start

:Recebe Evento Captura;

:Recupera Fonte de Video em Repositorio;
note right: Repository::VideoSource()

:Captura Video;
note right: VideoSource::video()

:Armazena Video;
note right: Storage::write()

:Informa disponibilidade de video;
note right: Channel::send()

stop
@enduml

```
