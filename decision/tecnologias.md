# Registro de Decisão Arquitetural (ADR)

## Título: Arquitetura do Sistema de Captura de Vídeo com Comunicação Assíncrona e Atualização em Tempo Real - Revisão 10

**Status:** Aceito

**Contexto:**

O sistema de captura de vídeo tem como objetivo permitir que os usuários solicitem o download e processamento de vídeos de URLs externas, disponibilizando-os em uma plataforma centralizada. Além disso, o sistema deve fornecer um portal de gerenciamento para que os administradores possam monitorar o progresso das capturas de vídeo em tempo real. A arquitetura do sistema deve ser escalável, resiliente e eficiente, utilizando tecnologias como RabbitMQ para comunicação assíncrona entre os componentes e Server-Sent Events (SSE) para atualizações em tempo real no portal do administrador.

Para simplificar a arquitetura e otimizar o fluxo de eventos, decidimos unificar a fila `queue.captura.output` com o tópico `topic.captura.events`, utilizando este último como o ponto central para a publicação e consumo de eventos relacionados ao resultado do processamento dos vídeos.

**Decisão:**

A arquitetura proposta para o sistema de captura de vídeo é baseada em uma abordagem de microsserviços, utilizando os seguintes componentes e tecnologias:

* **API Gateway (gateway):** Responsável por receber as requisições HTTP dos usuários e encaminhá-las para os microsserviços apropriados.
* **Evento Captura Input (Backend) (eventCapturaInputBackend):** Função AWS Lambda responsável por receber as solicitações de captura de vídeo dos usuários, validar os dados e publicar as mensagens na fila `queue.captura.input`.
* **Evento Captura Output (Backend) (eventCapturaOutputBackend):** Função AWS Lambda responsável por consumir as mensagens do tópico `topic.captura.events`, filtrando apenas os eventos com a chave de roteamento `video.captura.output`, processar os resultados das capturas de vídeo e atualizar o banco de dados.
* **Captura Vídeo (Backend) (capturaVideoBackend):** Projeto Java responsável por consumir as mensagens da fila `queue.captura.input`, realizar o download e processamento dos vídeos e publicar os resultados no tópico `topic.captura.events`.
* **Portal (Frontend) (portalFrontendAdmin):** Interface web do portal de gerenciamento, responsável por exibir as informações das capturas de vídeo em tempo real para os administradores.
* **Portal (Backend) (portalBackendAdmin):** Microsserviço responsável por fornecer os dados das capturas de vídeo para o Portal (Frontend), consumindo as mensagens do tópico `topic.captura.events`.
* **RabbitMQ:** Sistema de mensageria utilizado para comunicação assíncrona entre os microsserviços, garantindo o desacoplamento entre eles e a escalabilidade do sistema.
  * **queue.captura.input:** Fila utilizada para armazenar as solicitações de captura de vídeo.
  * **topic.captura.events:** Tópico utilizado para publicar e consumir os eventos relacionados às capturas de vídeo, incluindo os resultados do processamento.
* **Server-Sent Events (SSE):** Tecnologia utilizada para enviar as atualizações em tempo real do Portal (Backend) para o Portal (Frontend).

**Diagrama de Arquitetura:**

```plantuml
@startuml
left to right direction

actor User as user
actor Admin as admin

component "Api Gateway" as gateway
component "Evento Captura Input (Backend) (AWS Lambda)" as eventCapturaInputBackend
component "Evento Captura Output (Backend) (AWS Lambda)" as eventCapturaOutputBackend

package "RabbitMQ" {
queue "topic.captura.events" as tce
queue "queue.captura.input" as qci
}

package "Internal" {
component "Portal (Frontend)" as portalFrontendAdmin
component "Portal (Backend)" as portalBackendAdmin
component "Captura Video (Backend) (Java)" as capturaVideoBackend
}

user --> gateway : HTTPS/GET
gateway --> eventCapturaInputBackend : Lambda/GET
eventCapturaInputBackend --> qci: Pub
capturaVideoBackend ..> qci: Sub
capturaVideoBackend --> tce : Pub
portalFrontendAdmin -> portalBackendAdmin
portalBackendAdmin .left.> tce: Sub
admin --> portalFrontendAdmin : SSE

eventCapturaOutputBackend .up.> tce: Sub (Filtra por video.captura.output)
@enduml
```

**Consequências:**

* **Positivas:**
  * **Simplificação da Arquitetura:** A unificação da fila e do tópico reduz a complexidade do sistema.
  * **Otimização do Fluxo de Eventos:** O uso de um único tópico para os resultados do processamento facilita o gerenciamento e a escalabilidade.
  * **Manutenção de Benefícios Anteriores:** A arquitetura mantém as vantagens de escalabilidade, resiliência, atualização em tempo real e desacoplamento.
  * **Redução de Custos:** A utilização de funções AWS Lambda e a otimização da arquitetura continuam a contribuir para a redução dos custos de infraestrutura.

* **Negativas:**
  * **Aumento da Complexidade do eventCapturaOutputBackend:** O microsserviço agora precisa implementar a lógica de filtragem dos eventos.
  * **Potencial Aumento no Tráfego do Tópico:** Se o volume de eventos não relacionados ao output for muito alto, pode haver um aumento no tráfego do tópico.

**Mitigação de Riscos:**

* **Complexidade do eventCapturaOutputBackend:** Utilizar um framework de desenvolvimento que facilite a implementação da lógica de filtragem, como filtros baseados em routing keys no RabbitMQ.
* **Aumento no Tráfego do Tópico:** Monitorar o tráfego do tópico e, se necessário, implementar estratégias de otimização, como a criação de tópicos separados para diferentes tipos de eventos.

**Data da Decisão:** 28/05/2024

**Responsável pela Decisão:** Jefferson Lisboa
