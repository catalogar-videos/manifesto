# Registro de Decisão Arquitetural (ADR)

**Título:** Estrutura de Dependências do Componente "Capturar Vídeo"

**Status:** Aprovado

**Contexto:**

O componente "Capturar Vídeo" é responsável por baixar vídeos de diferentes servidores. Para garantir a flexibilidade, a reutilização de código e a facilidade de manutenção, é necessário definir uma estrutura de dependências que permita adicionar novos servidores facilmente e evitar a duplicação de código.

**Decisão:**

A estrutura de dependências do componente "Capturar Vídeo" será a seguinte:

* O componente "Capturar Vídeo" dependerá de bibliotecas externas que implementam a lógica de download de vídeos de servidores específicos (por exemplo, "Biblioteca Server A" e "Biblioteca Server B"). Essas bibliotecas serão desenvolvidas e mantidas pela equipe de desenvolvimento.
* O componente "Capturar Vídeo" também dependerá de uma biblioteca central chamada "Capturar Vídeo Core", que conterá a lógica comum de download de vídeos, como validação de URLs, gerenciamento de erros e comunicação com as bibliotecas de servidor.

```plantuml
@startuml

component "Capturar Vídeo" as Capturar
component "Biblioteca Server A" as ServerA
component "Biblioteca Server B" as ServerB
component "Biblioteca Video Core" as ServerCore

Capturar --> ServerA
Capturar --> ServerB
ServerA ..> ServerCore : dependência
ServerB ..> ServerCore : dependência

@enduml
```

**Consequências:**

* **Positivas:**
  * **Reutilização de código:** A lógica comum de download de vídeos será centralizada em "Capturar Video Core", evitando a duplicação de código.
  * **Facilidade de manutenção:** Alterações na lógica de download poderão ser feitas em um único lugar ("Capturar Video Core"), facilitando a manutenção do código.
  * **Flexibilidade:** Novos servidores poderão ser adicionados facilmente, criando novas dependências que utilizam "Capturar Video Core".
  * **Encapsulamento:** As bibliotecas de servidor encapsulam a lógica específica de cada servidor, tornando o código mais modular e fácil de testar.
* **Negativas:**
  * **Gerenciamento de dependências:** A equipe de desenvolvimento precisará gerenciar as dependências das bibliotecas de servidor.
  * **Potencial de conflitos:** Pode haver conflitos entre as bibliotecas de servidor se elas não forem projetadas para trabalhar juntas.

**Alternativas Consideradas:**

* **Implementar toda a lógica de download no componente "Capturar Vídeo":** Essa alternativa foi descartada, pois levaria à duplicação de código, dificultaria a manutenção e tornaria o componente "Capturar Vídeo" muito complexo.
* **Usar um framework de download de vídeos:** Essa alternativa foi considerada, mas a criação de bibliotecas personalizadas permite maior flexibilidade e controle sobre o processo de download.

**Data da Decisão:** 26/05/2024

**Responsável pela Decisão:** Jefferson Lisboa
