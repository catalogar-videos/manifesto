# Catalogar Vídeo: Documento Aprimorado e Analisado

**Data Inicial:** 24/05/2024

**Data de Revisão:** 26/05/2024

**Autor:** Jefferson Lisboa

**Nome do Projeto:** Catalogar Vídeo

## Objetivo do Projeto

O projeto "Catalogar Vídeo" visa permitir o download de vídeos de URLs externas para uma plataforma centralizada, otimizando o processo e oferecendo ferramentas de gerenciamento. Usuários podem baixar vídeos, escolher servidores e acompanhar o progresso, enquanto administradores gerenciam vídeos e configurações.

## Casos de Uso

| ID                     | Caso de Uso                     | Descrição                                                                                                                                                                                                                                      |
| ---------------------- | ------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| [UC1](usecases/UC1.md) | Capturar Vídeo                  | O usuário insere os parâmetros obrigatórios para a captura de um novo vídeo no Portal do Usuário, seleciona o servidor de download, e o sistema baixa e armazena o vídeo, notificando o usuário sobre o progresso e a conclusão.               |
| [UC2](usecases/UC2.md) | Pesquisar e Visualizar Catálogo | O usuário pesquisa vídeos por título ou descrição no Portal do Usuário, visualiza a lista de resultados e seleciona um vídeo para assistir.                                                                                                    |
| [UC3](usecases/UC3.md) | Organizar e Gerenciar Vídeos    | O administrador pode excluir vídeos, editar metadados (título, descrição, tags), organizar vídeos em playlists e compartilhar playlists com outros usuários.                                                                                   |
| [UC4](usecases/UC4.md) | Selecionar Opções de Servidores | O usuário pode visualizar e escolher entre diferentes servidores de download, com informações sobre velocidade, disponibilidade e outros critérios relevantes. A escolha do servidor impacta diretamente a velocidade e o sucesso do download. |
| [UC5](usecases/UC5.md) | Acompanhar Eventos de Captura   | O administrador pode visualizar informações sobre cada etapa do processo de captura de vídeo: solicitação, progresso, armazenamento e disponibilidade do vídeo. O histórico também registra eventuais erros ocorridos durante o download.            |

## Diagrama de Casos de Uso

```plantuml
@startuml

left to right direction

actor Usuário
actor Administrador

rectangle "Casos de Uso" {
    usecase (UC1) as "Capturar Vídeo "
    usecase (UC2) as "Pesquisar e Visualizar Catálogo"
    usecase (UC3) as "Organizar e Gerenciar Vídeos"
    usecase (UC4) as "Selecionar Opções de Servidores"
    usecase (UC5) as "Acompanhar Eventos de Captura"
}

Usuário -- (UC1)
UC4 -up-> UC1 : dependente
Usuário -- (UC2)
Administrador -up- (UC5)
Administrador -up- (UC3)
Administrador -up- (UC4)

@enduml
```

## Diagrama de Componentes

### Diagrama: Usuário para o Portal do Usuário

```plantuml
@startuml
left to right direction

actor "Usuário" as user

package "Interface do Usuário" {
    component "Portal do Usuário" as portal
}

user --> portal : Solicita captura ou visualização de vídeo
user --> portal : Solicita visualização dos eventos de captura

@enduml
```

### Diagrama: Administrador para o Portal de Gerenciamento

```plantuml
@startuml
left to right direction

actor "Administrador" as admin

package "Portal de Gerenciamento" {
    component "Portal de Gerenciamento" as portal
}

admin --> portal : Solicita visualização dos eventos de captura
portal ..> admin : eventos
@enduml
```

### Diagrama: Portal do Usuário para Catálogo de Vídeo

```plantuml
@startuml
left to right direction

package "Interface do Usuário" {
    component "Portal do Usuário" as portal
}

package "Backend" {
    component "Catálogo de Vídeo" as catalogo
    database "Banco de Dados" as db
    queue "Fila de Eventos" as message_queue
}

portal --> catalogo: Solicitar captura de novo vídeo
portal --> catalogo : Solicita informações dos servidores e lista de vídeos
portal <.. catalogo : Retorna informações dos servidores e lista de vídeos
catalogo --> db : Persiste informações do vídeo
catalogo --> message_queue : Publica evento de conclusão de download

@enduml
```

### Diagrama: Catálogo de Vídeo para Capturar Vídeo

```plantuml
@startuml

left to right direction

package "Backend" {
    component "Catálogo de Vídeo" as catalogo
}

package "Processamento de Vídeo" {
    component "Capturar Vídeo" as capturar
    database "Cache" as cache
}

cloud {
    database "Servidor de Vídeo" as server
    database "Armazenamento de Vídeo" as storage
    queue "Fila de Eventos" as eventos
}

catalogo --> capturar : Solicita download de novo vídeo
capturar --> server : Baixa o vídeo
capturar -left-> cache : Gerencia o estado do download
capturar ..> storage : Armazena o vídeo baixado
capturar ..> eventos : Envia eventos das ações de captura do vídeo
capturar .up.> catalogo : Evento: Conclusão do Download

@enduml
```

## Problemas Potenciais

* **Escalabilidade:** O sistema pode ter dificuldades em lidar com um grande número de usuários ou vídeos, especialmente se o armazenamento e o processamento não forem projetados para escalar.
* **Segurança:** É crucial garantir a segurança dos dados dos usuários e dos vídeos armazenados, implementando medidas de proteção contra acesso não autorizado, perda de dados e ataques maliciosos.
* **Disponibilidade:** O sistema deve ser altamente disponível para garantir que os usuários possam acessar e baixar vídeos sem interrupções. Isso pode envolver o uso de servidores redundantes, balanceamento de carga e outras técnicas.
* **Qualidade do Vídeo:** A qualidade dos vídeos baixados pode variar dependendo da fonte e do servidor de download. É importante fornecer opções para que os usuários escolham a qualidade desejada e garantir que os vídeos sejam reproduzidos sem problemas.
* **Direitos Autorais:** O sistema deve estar em conformidade com as leis de direitos autorais, garantindo que os usuários baixem apenas vídeos que tenham permissão para usar ou que sejam de domínio público.
