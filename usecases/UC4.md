# Caso de Uso: Selecionar Opções de Servidores

**Ator Primário:** Usuário

**Resumo:**

O usuário obtém a lista de servidores disponíveis para baixar um vídeo antes de iniciar o processo de download.

**Pré-condições:**

* O usuário está autenticado no sistema (opcional, dependendo dos requisitos do sistema).
* O usuário está na página de download de vídeo.

**Pós-condições:**

* A lista de servidores habilitados para download é exibida para o usuário.

**Fluxo Principal:**

1. O usuário acessa a página de download de vídeo.
2. O sistema solicita ao serviço de Catálogo de vídeo a lista de servidores habilitados.
3. O serviço de Catálogo de vídeo retorna a lista de servidores habilitados para o sistema.
4. O sistema exibe a lista de servidores habilitados para o usuário.

**Fluxos Alternativos:**

* **3a. Nenhum servidor disponível:**
    1. O serviço de Catálogo de vídeo não retorna nenhum servidor habilitado.
    2. O sistema exibe uma mensagem de erro informando que não há servidores disponíveis para download no momento.

**Requisitos Especiais:**

* O sistema deve ser capaz de se comunicar com o serviço de Catálogo de vídeo para obter a lista de servidores habilitados.

**Regras de Negócio:**

* A lista de servidores habilitados pode ser filtrada com base em critérios como localização geográfica do usuário ou tipo de vídeo.

**Diagrama de Sequência:**

```plantuml
@startuml
autonumber

actor "Usuário" as user
participant "Portal do Usuário" as portal
participant "Catálogo de vídeo" as catalog

user -> portal: Acessa a página de download de vídeo
activate portal
portal -> catalog: Solicita servidores habilitados
activate catalog
catalog --> portal: Retorna servidores habilitados
deactivate catalog
portal --> user: Exibe lista de servidores habilitados
deactivate portal

@enduml
```

**Metadado JSON de Saída (Resposta da Lista de Servidores):**

```json
{
  "servidores": [
    {
      "nome": "server-X",
      "parametrosObrigatorios": ["id", "name"]
    },
    {
      "nome": "server-Y",
      "parametrosObrigatorios": ["token", "formato"]
    }
  ]
}
```
