# UC5: Acompanhar Eventos de Captura de Vídeo

**Descrição:**

Este caso de uso permite que o administrador acompanhe o progresso da captura de um vídeo, desde a solicitação inicial até a conclusão ou eventual erro. O sistema fornece informações detalhadas sobre cada etapa do processo, incluindo:

* **Solicitação:** Momento em que o administrador solicita a captura do vídeo, com detalhes como a URL do vídeo e o servidor de download escolhido.
* **Início:** Momento em que o download do vídeo é iniciado.
* **Progresso:** Atualizações periódicas sobre o andamento do download, como a porcentagem concluída e a velocidade de download.
* **Armazenamento:** Confirmação de que o vídeo foi armazenado com sucesso no sistema.
* **Disponibilidade:** Notificação de que o vídeo está disponível para visualização.
* **Erros:** Em caso de falha em alguma etapa do processo, o sistema registra o erro e informa o administrador sobre o problema.

O administrador pode acessar o histórico de eventos de captura de vídeo através de uma interface no Portal do administrador. Essa interface exibe os eventos em ordem cronológica, com detalhes sobre cada etapa. O administrador pode filtrar os eventos por vídeo, data ou status (concluído, em andamento, erro).

**Fluxo Principal:**

1. O administrador acessa a seção "Histórico de Captura de Vídeo" no Portal do administrador.
2. O sistema exibe a lista de eventos de captura de vídeo do administrador, ordenados por data.
3. O administrador pode clicar em um evento para visualizar seus detalhes.
4. O sistema exibe os detalhes do evento, incluindo informações sobre cada etapa do processo.

**Fluxos Alternativos:**

* **Nenhum evento encontrado:** Se o administrador não tiver nenhum evento de captura de vídeo, o sistema exibe uma mensagem informando que não há histórico disponível.
* **Erro ao carregar eventos:** Se ocorrer um erro ao carregar os eventos, o sistema exibe uma mensagem de erro e solicita que o administrador tente novamente mais tarde.

**Exceções:**

* **Acesso não autorizado:** Se o administrador não estiver autenticado, o sistema o redireciona para a página de login.

## Exemplo de Saída (JSON)

```json
{
  "videoId": "12345",
  "videoUrl": "https://www.example.com/video.mp4",
  "server": "Server A",
  "events": [
    {
      "timestamp": "2024-05-26T10:30:00Z",
      "type": "REQUESTED",
      "message": "Solicitação de captura de vídeo recebida."
    },
    {
      "timestamp": "2024-05-26T10:31:00Z",
      "type": "STARTED",
      "message": "Download do vídeo iniciado."
    },
    {
      "timestamp": "2024-05-26T10:35:00Z",
      "type": "PROGRESS",
      "message": "Download em progresso: 50% concluído."
    },
    {
      "timestamp": "2024-05-26T10:40:00Z",
      "type": "COMPLETED",
      "message": "Download do vídeo concluído."
    },
    {
      "timestamp": "2024-05-26T10:41:00Z",
      "type": "STORED",
      "message": "Vídeo armazenado com sucesso."
    },
    {
      "timestamp": "2024-05-26T10:42:00Z",
      "type": "AVAILABLE",
      "message": "Vídeo disponível para visualização."
    }
  ]
}
```

## Diagrama de Sequência

```plantuml
@startuml
autonumber

actor administrador
participant "Portal do administrador" as UI
participant "Catálogo de Vídeo" as Catalog
participant "Tópico de Eventos" as EventTopic

administrador -> UI: Acessar Histórico de Captura
UI -> Catalog: Abrir Conexão SSE (videoId)
Catalog -> EventTopic: Abrir canal (Stream)
EventTopic --> Catalog: Consume Eventos (Stream)
Catalog --> UI: Enviar Eventos (SSE)
UI -> administrador: Exibir Eventos (Atualização em Tempo Real)
@enduml

```
