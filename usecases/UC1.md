# Caso de Uso: Capturar Vídeo

**Ator Primário:** Usuário

**Resumo:**

O usuário baixa um novo vídeo para sua biblioteca, escolhendo o servidor de download de sua preferência e fornecendo os parâmetros obrigatórios para o servidor selecionado.

**Pré-condições:**

* O usuário está autenticado no sistema.
* O usuário está na página de download de vídeo.

**Pós-condições:**

* Um evento é enviado para o componente Capturar Vídeo para iniciar o download do vídeo, incluindo os parâmetros do servidor.

**Fluxo Principal:**

1. O usuário acessa a página de download de vídeo.
2. O sistema solicita a lista de servidores habilitados para download e seus parâmetros obrigatórios.
3. O sistema exibe a lista de servidores habilitados e seus parâmetros para o usuário.
4. O usuário seleciona um servidor da lista.
5. O usuário informa os parâmetros obrigatórios para o servidor selecionado.
6. O sistema envia uma requisição HTTP para o Catálogo de Vídeo com o identificador do servidor escolhido e os parâmetros do servidor.
7. O Catálogo de Vídeo encaminha um evento para o componente Capturar Vídeo, contendo o identificador do servidor escolhido e seus respectivos parâmetros.

**Fluxos Alternativos:**

* **4a. Nenhum servidor disponível:**
    1. O sistema exibe uma mensagem de erro informando que não há servidores disponíveis.
    2. O sistema impede o usuário de prosseguir com o download.
* **5a. Parâmetros inválidos:**
    1. O sistema exibe uma mensagem de erro informando quais parâmetros são inválidos ou faltantes.
    2. O sistema impede o usuário de prosseguir com o download até que os parâmetros sejam corrigidos.

**Requisitos Especiais:**

* O sistema deve ser capaz de se comunicar com o serviço Catalógo de Vídeo para obter a lista de servidores habilitados e seus parâmetros obrigatórios.
* O sistema deve ser capaz de validar os parâmetros informados pelo usuário de acordo com os requisitos de cada servidor.
* O sistema deve ser capaz de enviar eventos para o componente Capturar Vídeo utilizando um sistema de mensageria, incluindo os parâmetros do servidor.

**Regras de Negócio:**

* O usuário só pode baixar vídeos de servidores habilitados.
* O usuário deve fornecer todos os parâmetros obrigatórios para o servidor selecionado.

**Diagrama de Sequência:**

```plantuml
@startuml
autonumber

actor "Usuário" as user
participant "Portal do Usuário" as portal
participant "Catálogo de Vídeo" as catalogo
participant "Capturar Vídeo" as capturar
user -> portal: Acessa a página de download de vídeo
activate portal
portal -> catalogo: Solicita servidores habilitados
activate catalogo
catalogo --> portal: Retorna servidores
deactivate catalogo
portal -> user: Exibe lista de servidores
user -> portal: Seleciona um servidor
user -> portal: Solicita de captura de novo vídeo
portal -> catalogo: Solicita de captura de novo vídeo (servidor e parâmetros)
activate catalogo
catalogo -> capturar: Inicia captura de vídeo
deactivate catalogo
deactivate portal

@enduml
```

**Metadados :**

**Saída Servidores disponiveis (Catálogo de Vídeo para Portal do Usuário):**

```json
{
  "servidores": [
    {
      "nome": "server-X",
      "parametrosObrigatorios": ["id", "name"]
    },
    {
      "nome": "server-Y",
      "parametrosObrigatorios": ["token", "formato"]
    }
  ]
}
```

**Entrada Captura de novo vídeo (Portal do Usuário para Catálogo de Vídeo):**

```json
{
  "servidor": "server-X",
  "parametros": {
    "id": "abcdefg",
    "name": "video_exemplo"
  }
}
```

**Saída Captura de novo vídeo (Catálogo de Vídeo para Portal do Usuário):**

```json
{
  "evento": "download_iniciado",
  "video_id": "123",
  "timestamp": "2023-11-25T15:30:00Z"
}
```
